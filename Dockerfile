FROM ubuntu:16.04
MAINTAINER mickael.kerjean@gmail.com

RUN apt-get -y update && \
    apt-get install -y openssh-client && \
    mkdir ~/.ssh && \
    echo "Host *" > ~/.ssh/config && \
    echo "StrictHostKeyChecking no" >> ~/.ssh/config && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD eval $(ssh-agent -s)
# Introduction
Establishing an ssh connection on gitlab CI.

# How to?
1) create a set of public/private key on your machine
```
ssh-keygen -t rsa -C "your_email@example.com"
```
2) copy the public key (~/.ssh/id_rsa.pub) on the machine you want to connect at the following location ~/.ssh/authorized_keys
```
$USERNAME=root
$HOST=example.com
scp ~/.ssh/id_rsa.pub $USERNAME@$HOST:~/
ssh $USERNAME@$HOST -e "cat ~/id_rsa.pub >> ~/ssh.authorized_keys && rm ~/id_rsa.pub"
```
3) push the private key as a environment variable on gitlab
Go to your gitlab project page, then:
```
Project settings -> CI/CD pipelines -> Secret Variable

Add a key called: SSH_PRIVATE_KEY
with the value of your private key
```

4) In your CI pipeline, reference the machines/ssh image like done [here](https://gitlab.com/mickael.kerjean/ci-ssh/blob/master/.gitlab-ci.yml)



